<?php


  $country_edit = "category-edit";


include('header.php');




$id = $_GET['id'];

  $sel_qur = "SELECT country.* , configuration.currency_name , configuration.currency_symbol
  FROM Country as country
  INNER JOIN Configuration as configuration ON configuration.country_id = country.id
  where country.id = '" . $id . "'";
  $sel_run = queryRunner($sel_qur);
  while ($sel_data = rowRetriever($sel_run)){
      $country_name = $sel_data['name'];
      $country_per = $sel_data['percentage'];
      $currency_name = $sel_data['currency_name'];
      $currency_symbol = $sel_data['currency_symbol'];
  }


?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header category-header">

           <?php

            include('header-bar.php');

            ?> 


        </header><!-- /header -->
        <!-- Header-->



        <div class="category-table-content col-sm-12">

            <div class="col-sm-12">
                
              <div class="card">
                <div class="card-body cat-card-body">

                  <form role="form" method="post" action="country-actions.php?id=<?php echo $id ?>" enctype="multipart/form-data">

                    <div class="row form-group category-table">
                      <div class="col col-12 col-sm-7">
                        <input type="text" name="country_name" placeholder="Enter Country Name" class="form-control" value="<?php echo $country_name; ?>" required>
                      </div>
                    </div>
                    
                     <div class="row form-group category-table">
                      <div class="col col-12 col-sm-7">
                        <input type="text" name="country_percentage" placeholder="Enter Country Percentage" class="form-control" value="<?php echo $country_per; ?>" required>
                      </div>
                    </div>
                    
                    <div class="row form-group category-table">
                      <div class="col col-12 col-sm-4">
                        <input type="text" name="currency_name" placeholder="Enter Current Name" class="form-control" value="<?php echo $currency_name; ?>" required>
                      </div>
                      <div class="col col-12 col-sm-3">
                        <input type="text" name="currency_symbol" placeholder="Enter Currency Symbol" class="form-control" value="<?php echo $currency_symbol; ?>" required>
                      </div>
                    </div>
                    
                    
                    <div class="row form-group category-table">
                      <div class="col col-12 col-sm-7">
                        <input type="submit" class="btn btn-theme cate-btn text-light" id="country_edit" name="country_edit" value="Update Country">
                      </div>
                    </div>
                  </form>

                  
                </div>
              </div>

            </div>

        </div>





    </div><!-- /#right-panel -->

    <!-- Right Panel -->

<?php

    include('footer.php');

?>

